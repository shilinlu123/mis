##############################################################
# Homework #3
# name: Shi Lin Lu
# sbuid: 108133356
##############################################################
.text

##############################
# Part 1 FUNCTION
##############################

arrayFill:
	#Define your code here
	move $t0,$a0  # move arr into $t0
	move $t1,$a1  # move fileName into $t1
	move $t2,$a2  # move byte number into $t2
	
	la $a0,($t1)  # file name
	li $a1,0      # flag is 0
	li $a2,0      # mode is 0
	li $v0,13     # open file
	syscall
	
	move $a0,$v0  # file descriptor
	la $a1,0xffff0000  # text space (arr)
	la $a2,($t2)  # read one byte at a time
	

	li $v0,14     # read file
	syscall 	
	
	#li $v0,16
	#syscall       # close the file
	
	#la $a0,($a1)
	#li $v0,4
	#syscall
	
	#li $t4,45
	#sw $t4,0xffff0000
	#lw $t6,0xffff0000
	#la $a0,($t6)
	#li $v0,1
	#syscall
	
	jr $ra

find2Byte:
	#Define your code here
	move $t0,$a0    # @param arr Base address of the array in memory.
	move $t1,$a1    # @param value The 2-byte value to search for in the array.
	move $t2,$a2    # @param row The number of rows in the array.
	move $t3,$a3    # @param column The number of columns in the array.
	addi $t2,$t2,-2
	addi $t3,$t3,-2
	li $t4,0   # row counter
	li $t5,0   # column counter
	
	lhu $t6,($t0)  # the array      
	lhu $t7,($t1)  # the 2 byte search

again:  lhu $t6,($t0)  # the array  
	beqz $t6,nexist	
	
	beq $t7,$t6,found
	bne $t7,$t6,nfound

found:  #la $a0,yes
        #li $v0,4
	#syscall	
	j done

nfound: #la $a0,no
	#li $v0,4
	#syscall	
	addi $t0,$t0,2   # search the next 2 bytes in array
	ble $t5,$t3,column
	bgt $t5,$t3,row
column:	addi $t5,$t5,1  #  search next column
	j again
row:    addi $t4,$t4,1  # search next row
	li $t5,0	
	j again
#nexist: li $v0,-1     # return -1 if not exist   # row
	#li $v1,-1     # return -1 if not exist   # column
	#move $t7,$v0
	#move $t6,$v1
	#la $a0,($t7)
	#li $v0,1
	#syscall
	#la $a0,($t6)
	#li $v0,1
	#syscall
done:	move $v0,$t4
	move $v1,$t5
	j realdone
nexist: li $v0,-1     # return -1 if not exist   # row
	li $v1,-1     # return -1 if not exist   # column	
	
	
	#la $a0,($t4)    # print row
	#li $v0,1
	#syscall
	#la $a0,($t5)    # print column
	#li $v0,1
	#syscall
	
realdone:move $t7,$v0
	move $t6,$v1
	#la $a0,($t7)
	#li $v0,1
	#syscall
	#la $a0,($t6)
	#li $v0,1
	#syscall	
	 jr $ra

##############################
# PART 2/3 FUNCTION
##############################

playGame:
	#Define your code here
	move $t0,$a0     # @param arr Base address of the array in memory.
	move $t1,$a1    # @param start_r The row of the mower.
	move $t2,$a2     # @param start_c The column of the mower.
	move $t3,$a3     # @param moves Address of string of characters for moves.
	
	beq $t1,-1,notfound   # if the row of the mower is -1, tat means mower was not found
	li $t4,80    # 80 columns
	li $t5,2    # 2 bytes per spot
	mul $t6,$t4,$t5 
	mul $t6,$t6,$t1   # 80 column * 2 bytes per spot * row of the mower  ( This finds the row)
	mul $t7,$t2,$t5   # column of the mower * 2 bytes per spot  ( This finds the column)
	add $t7,$t7,$t6   # find the address of the mower
	add $t0,$t0,$t7   # the address mower
	li $t9,0x20       # space character
	li $t2,0xAF       # light green background  white word     ( mowed the grass) 
	li $t8,0x2B       # + sign
	li $t7,0x2F      # dark green background  white word 
	li $t6,0xB06F    # flowers
	j moveagain
notfound: li $t9,0x20       # space character
	  li $t2,0xAF       # light green background  white word     ( mowed the grass) 
	  li $t8,0x2B       # + sign
	  li $t7,0x2F      # dark green background  white word    
	  li $t6,0xB06F    # flowers
	 # sb $t8,($t0)     # + sign on a green background represents the mower
	 # sb $t7,1($t0)	
	  li $a1,0    # if mower is not found, set row to 0
	  li $a2,0   # if mower not found, set column to 0
	
moveagain:	
	
	lb $t1,($t3)      # load first byte of the string of character 
	beqz $t1,finish
	beq $t1,'a',moveleft
	beq $t1,'d',moveright
        beq $t1,'w',moveup
        beq $t1,'s',movedown
	#bne $t1,'a',nextchar

nextchar: addi $t3,$t3,1   # next character to move
	  j moveagain
moveleft: beq $a2,0,edgeL   # if mower reaches column 0 (edge case)
	  addi $t0,$t0,-2   # gets the next block
	  lhu $t5,($t0)
	  addi $t0,$t0,2      # restore the address
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	  beq $t5,$t6,collision       # checks for collision of flowers
	   
	   sb $t9,($t0)
	  addi $t0,$t0,-2 ###
	  lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,2  ###
	  
	  ori $t4,$t4,128  # makes it bold color       # mower is FKEN GREEN
	  sb $t4,1($t0)
	  # sb $t2,1($t0)   # light green background  white word     ( mowed the grass) 
	  addi $t0,$t0,-2	
	  sb $t8,($t0)     # + sign on a green background represents the mower
	  sb $t7,1($t0)
	  la $a0,500 
	   li $v0,32     # tells the thread to sleep
	   syscall
	  addi $t3,$t3,1   # next character to move
	  addi $a2,$a2,-1  # decrement column counter
	  j moveagain
edgeL:    addi $t0,$t0,158   # gets the next block
	  lhu $t5,($t0)
	  addi $t0,$t0,-158
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	  beq $t5,$t6,collision    # checks for collision of flowers
	  sb $t9,($t0)    # space character
	  addi $t0,$t0,158
	  lb $t4,1($t0)   # gets the color byte
	   addi $t0,$t0,-158
	  ori $t4,$t4,128  # makes it bold color
	  sb $t4,1($t0)
	  #sb $t2,1($t0)   # light green background  white word     ( mowed the grass) 	
	  addi $t0,$t0,158	
	  sb $t8,($t0)     # + sign
	  sb $t7,1($t0)	  # dark green background  white word    
          la $a0,500 
	  li $v0,32     # tells the thread to sleep
	  syscall
	  addi $t3,$t3,1   # next character to move
	  li $a2,79      # reset counter to 79
	  j moveagain 
moveright:beq $a2,79,edgeR   # if mower reaches column 79 (edge case)
	 
	  addi $t0,$t0,2   # gets the next block
	  lhu $t5,($t0)
	 addi $t0,$t0,-2
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	 beq $t5,$t6,collision    # checks for collision of flowers
	  sb $t9,($t0)    # space character
	  addi $t0,$t0,2 ###
	  lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,-2  ###
	  
	  ori $t4,$t4,128  # makes it bold color       # mower is FKEN GREEN
	  sb $t4,1($t0)
	 
	  addi $t0,$t0,2	
	  
	  sb $t8,($t0)     # + sign
	  sb $t7,1($t0)	  # dark green background  white word    
	  la $a0,500 
	   li $v0,32     # tells the thread to sleep
	   syscall
	  addi $t3,$t3,1   # next character to move
	  addi $a2,$a2,1   # increment column counter
	  
	  j moveagain
collision: la $a0,detected      #  IT MOWED THE FKEN BACK LAWN
	   li $v0,4
	   syscall
	   addi $t3,$t3,1   # next character to move
	   
	   j moveagain

edgeR:    addi $t0,$t0,-158   # gets the next block
	  lhu $t5,($t0)
	  addi $t0,$t0,158
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	 beq $t5,$t6,collision    # checks for collision of flowers
	  sb $t9,($t0)    # space character
	   addi $t0,$t0,-158
	   lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,158
	  ori $t4,$t4,128  # makes it bold color
	  sb $t4,1($t0)
	  addi $t0,$t0,-158	
	  sb $t8,($t0)     # + sign
	  sb $t7,1($t0)	  # dark green background  white word    
          la $a0,500 
	  li $v0,32     # tells the thread to sleep
	  syscall
	  addi $t3,$t3,1   # next character to move
	  li $a2,0      # reset counter to 0
	  j moveagain
moveup:   beq $a1,0,edgeU
	  addi $t0,$t0,-160   # gets the next block
	  lhu $t5,($t0)
	 addi $t0,$t0,160
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	 beq $t5,$t6,collision    # checks for collision of flowers
	 sb $t9,($t0)
	  addi $t0,$t0,-2 ###
	  lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,2  ###
	  
	  ori $t4,$t4,128  # makes it bold color       # mower is FKEN GREEN
	  sb $t4,1($t0)	  
	 addi $t0,$t0,-160	
	sb $t8,($t0)
	 sb $t7,1($t0)	
         la $a0,500 
	   li $v0,32     # tells the thread to sleep
	   syscall
         addi $t3,$t3,1   # next character to move
	 addi $a1,$a1,-1  # decrement row counter
	 j moveagain
edgeU:    addi $t0,$t0,3840   # gets the next block
	  lhu $t5,($t0)
	 addi $t0,$t0,-3840
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	 beq $t5,$t6,collision    # checks for collision of flowers
	  sb $t9,($t0)    # space character
	 #sb $t2,1($t0)   # light green background  white word     ( mowed the grass) 
	  addi $t0,$t0,2
	   lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,-2
	  ori $t4,$t4,128  # makes it bold color
	  sb $t4,1($t0)
	  
	  addi $t0,$t0,3840	
	  sb $t8,($t0)     # + sign
	  sb $t7,1($t0)	  # dark green background  white word    
          la $a0,500 
	  li $v0,32     # tells the thread to sleep
	  syscall
	  addi $t3,$t3,1   # next character to move
	  li $a1,24      # reset counter to 24
	  j moveagain
movedown: beq $a1,24,edgeD
	  addi $t0,$t0,160   # gets the next block
	  lhu $t5,($t0)
	 addi $t0,$t0,-160
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	 beq $t5,$t6,collision    # checks for collision of flowers
	  sb $t9,($t0)
	  addi $t0,$t0,2 ###
	  lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,-2  ###
	  
	  ori $t4,$t4,128  # makes it bold color       # mower is FKEN GREEN
	  sb $t4,1($t0)	    
	  addi $t0,$t0,160	
	  sb $t8,($t0)       
	  sb $t7,1($t0)		
	  la $a0,500 
	   li $v0,32      # tells the thread to sleep
	   syscall
	  addi $t3,$t3,1   # next character to move
	  addi $a1,$a1,1  # increment row counter
	  j moveagain
edgeD:    addi $t0,$t0,-3840   # gets the next block
	  lhu $t5,($t0)
	  addi $t0,$t0,3840
	  beq $t5,0x3F20,collision    # checks for collision of dirt
	  beq $t5,0x7F52,collision    # checks for collision of rocks
	  beq $t5,0x4F20,collision    # checks for collision of water
	  beq $t5,0x2F5E,collision    # checks for collision of trees
	 beq $t5,$t6,collision    # checks for collision of flowers
	  sb $t9,($t0)    # space character
	   addi $t0,$t0,-2
	   lb $t4,1($t0)   # gets the color byte
	  addi $t0,$t0,2
	  ori $t4,$t4,128  # makes it bold color
	  sb $t4,1($t0)
	  
	  addi $t0,$t0,-3840	
	  sb $t8,($t0)     # + sign
	  sb $t7,1($t0)	  # dark green background  white word    
          la $a0,500 
	  li $v0,32     # tells the thread to sleep
	  syscall
	  addi $t3,$t3,1   # next character to move
	  li $a1,0      # reset counter to 24
	  j moveagain
finish:	
	
	jr $ra
.data
yes: .asciiz "Found"
no: .asciiz "Not Found"	
detected: .asciiz "Collision detected "
	
	#Define your memory here 
