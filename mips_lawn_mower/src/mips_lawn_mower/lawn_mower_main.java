.data 
arr: .byte 0:80
search:  .byte 2
.align 2
fileName: .asciiz "landscape1.map"
direction: .asciiz "dddwwwaaasss"


.text

.globl main

main:  la $a0,arr
       la $a1,fileName
       li $a2,4000
       jal arrayFill
 ###################################       
       la $a0,0xffff0000    # @param arr Base address of the array in memory.
       
       li $t0,'+'
       li $t1,'/'
       sb $t0,search
       sb $t1,search+1
       la $a1,search  # @param value The 2-byte value to search for in the array.
       
       li $a2,25    # @param row The number of rows in the array.

       li $a3,80    # @param column The number of columns in the array.
       #la $a0,search
       #li $v0,4
       #syscall
       jal find2Byte
########################################
        la $a0,0xffff0000    # @param arr Base address of the array in memory.
       	la $a1,($v0)        # @param start_r The row of the mower.
	la $a2,($v1)        # @param start_c The column of the mower.
        la $a3,direction   # @param moves Address of string of characters for moves.
        jal playGame
        
        
        #jal find2Byte
       # la $a0,0xffff0000    # @param arr Base address of the array in memory.
       #	#la $a1,($v0)        # @param start_r The row of the mower.
	#la $a2,($v1)        # @param start_c The column of the mower.
       # la $a3,direction   # @param moves Address of string of characters for moves.
        #jal playGame
       li $v0,10      # exit program
       syscall
      
.include "hw3.asm"



